const express = require('express')
const app = express();
const sholat = require("./source/controller/sholatService");
const quran = require("./source/controller/quranService");
const {
  cacheJadwal,
} = require("./source/services/api-request/api-jadwalsholat");
let cron = require("node-cron");
// const bodyParser = require("body-parser");
const PORT = 3542;

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

cron.schedule("0 0 * * *", () => {
  console.log("clear jadwal cache");
  cacheJadwal.length = 0;
});

app.route("/sholat/kota").get(sholat.getKota);
app.route("/sholat").get(sholat.getSholat);

app.route("/quran/surat").get(quran.getSurah);
app.route("/quran/list").get(quran.getSurahList);

app.listen(PORT, () => {
  console.log(`listening on ${PORT}`);
});
