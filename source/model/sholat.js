let DateServices = require("../services/dateServices")

module.exports = class Sholat{
    constructor(data){
        this.imsak = DateServices.dateConverter(data.tanggal, data.imsak).toISOString()
        this.subuh = DateServices.dateConverter(data.tanggal, data.subuh).toISOString()
        this.larangan_terbit = DateServices.dateConverter(data.tanggal, data.terbit).toISOString()
        this.dhuha = DateServices.dateConverter(data.tanggal, data.dhuha).toISOString()
        this.larangan_siang = DateServices.substractMinutes(DateServices.dateConverter(data.tanggal, data.dzuhur),30).toISOString()
        this.dzuhur = DateServices.dateConverter(data.tanggal, data.dzuhur).toISOString()
        this.ashar = DateServices.dateConverter(data.tanggal, data.ashar).toISOString()
        this.larangan_terbenam = DateServices.substractMinutes(DateServices.dateConverter(data.tanggal, data.maghrib),40).toISOString()
        this.maghrib = DateServices.dateConverter(data.tanggal, data.maghrib).toISOString()
        this.isya = DateServices.dateConverter(data.tanggal, data.isya).toISOString()
    }
}