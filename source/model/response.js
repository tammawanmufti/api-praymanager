module.exports = class Response{
    static errorResponse = (error) => new Response(false,"Oops, Terjadi kesalahan",error==null?[]:error);
    constructor(status,message,data){
        this.status = status;
        this.message = message;
        this.data = data;
    }
}

