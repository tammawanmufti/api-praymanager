module.exports = class SurahInfo{
    constructor(data){
        this.no = parseInt(data.nomor);
        this.nama = data.nama;
        this.arab = data.asma;
        this.total_ayat = parseInt(data.ayat);
        this.total_rukuk = parseInt(data.rukuk); 
    }
}