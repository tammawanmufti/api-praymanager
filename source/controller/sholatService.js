let Response = require("../model/response");
const { getJadwal } = require("../services/api-request/api-jadwalsholat");
const { getCity } = require("../services/api-request/api-geolocation");
const { getCode } = require("../services/api-request/api-jadwalsholat");

exports.getKota = async (req, res) => {
  try {
    let result = await getCode(
      await getCity(req.query.latitude, req.query.longitude)
    );
    res.send(new Response(true, "Berhasil mendapat kota", result));
  } catch (error) {
    res.send(Response.errorResponse(error));
  }
};

exports.getSholat = async (req, res) => {
  try {
    let result = await getJadwal(
      req.query.latitude,
      req.query.longitude,
      req.query.date
    );
    if(result!=null){
      res.send(new Response(true, "Berhasil mendapat jadwal", result));
    } else {
      throw "silahkan coba sesaat lagi"
    }
  } catch (error) {
    console.log(error)
    res.send(Response.errorResponse(error));
  }
};