const Response = require("../model/response")
const { getList }= require("../services/api-request/api-quran")
const SurahInfo = require("../model/surahInfo")

exports.getSurahList = async (req, res) => {
    let result = await getList();
    result = result.map(item => new SurahInfo(item))
    res.send(new Response(true,"Berhasil mendapat daftar surat",{total:result.length,daftar:result}));
}

exports.getSurah = async (req, res)=>{}