exports.dateConverter = (date,time)=>{
    let result = new Date(date+"T"+time+":00.000Z")
    return result;
}

exports.addMinutes = (date, minutes) => {
    return new Date(date.getTime() + minutes*60000);
}

exports.substractMinutes = (date, minutes) => {
    return new Date(date.getTime() - minutes*60000);
}