let axios = require("axios");
exports.getCity = async (latitude, longitude) => {
    let result = await axios.get(
      `https://api.bigdatacloud.net/data/reverse-geocode-client`,
      {
        params: {
          latitude: latitude,
          longitude: longitude,
          localityLanguage: "id",
        },
      }
    );
    // console.log(result.data);
    return { provinsi: result.data.principalSubdivision, kota: result.data.city };
  };
  