let axios = require("axios")
const { getCity } = require('./api-geolocation')
let Sholat = require("../../model/sholat")

exports.cacheCity = [];
exports.cacheJadwal = [];

exports.getCode = async (city) => {

    let result = await getID(city.kota)


    if (result.length < 1) {
        kota = city.kota.replace(" ", "")
        result = await getID(kota)
        if (result.length < 1) {
            result = await getID(city.provinsi);
        }
    }
    return result[0]
}

exports.getJadwal = async (latitude, longitude, date) => {
    let city = await getCity(latitude, longitude)
    let code = await this.getCode(city)
    let data = await (await getSholat(code, date)).data
    return { tanggal: date, lokasi: city, jadwal: data }
}

let getSholat = async (city, date) => {
    let cachedData = this.cacheJadwal.find(item => item.city === city && item.date === date);
    let parsedDate = date.split('-');
    if (cachedData != null) {
        return cachedData
    } else {
        let result = await axios.get(`https://api.myquran.com/v1/sholat/jadwal/${city.id}/${parsedDate[0]}/${parsedDate[1]}/${parsedDate[2]}`);
        let data = result.data.data.jadwal
        data.tanggal = date
        let res = { date: date, city: city, data: new Sholat(data) };
        this.cacheJadwal.push(res)
        return res
    }
}

let getID = async (city) => {

    try {
        city = city.toUpperCase()
        let data = this.cacheCity.find(item => item.nama == city);

        if (data != null) {
            return [data];
        } else {
            let result = await axios.get(
                `https://api.myquran.com/v1/sholat/kota/cari/${city}`
            )
            result.data.data.forEach((item) => {
                this.cacheCity.push(item)
            })

            return result.data.data
        }
    } catch (error) {

        throw "kota tidak ditemukan"
    }

};