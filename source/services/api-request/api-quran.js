let axios = require("axios")

const rawList = "https://api.banghasan.com/quran/format/json/surat";

exports.cacheSurahList = [];

exports.getList = async () => {
    if(this.cacheSurahList.length < 1){
        let result = await axios.get(rawList);
        this.cacheSurahList = result.data.hasil;
    }
    return this.cacheSurahList;
}

